/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2023. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggle').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
  });
  $('.navbar-collapse .close').on('click', function () {
    $('.navbar-collapse').removeClass('show');
  });
  // active navbar of page current
  var urlcurrent = window.location.href;
  $(".navbar-nav li a[href$='" + urlcurrent + "'],.nav-category li a[href$='" + urlcurrent + "']").addClass('active');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
      $('header').addClass('scroll');
    } else {
      $('header').removeClass('scroll');
    }
  });
});